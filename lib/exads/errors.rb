module Exads
  class HttpError < StandardError
    attr_reader :code, :headers, :response

    def initialize(code:, headers:, response:)
      @code = code
      @headers = headers
      @response = response
    end
  end
end
