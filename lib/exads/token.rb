module Exads
  class Token
    attr_reader :token_data, :expiration_time

    def token
      refresh_token unless valid?
      token_data['token']
    end

    private

    def valid?
      return false if token_data.nil?
      return false if expired?

      true
    end

    def expired?
      expiration_time <= Time.now
    end

    def refresh_token
      login_params = {
        username: Exads.configuration.username,
        password: Exads.configuration.password
      }

      @token_data = Exads.api_call(
        method: :post,
        path: '/login',
        params: login_params,
        use_token: false
      )
      @expiration_time = Time.now + @token_data['expires_in'].to_i
    end
  end
end
