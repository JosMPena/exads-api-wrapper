module Exads
  class Collection < Resource
    RESOURCE_PATH = '/collections'

    COLLECTIONS = %w(
      ad-exchange-partners ad-placement-types advertiser-ad-types
      publisher-ad-types admin-panel-column-settings balance-alert
      bidder-rule-settings browsers campaign-types carriers categories
      compliance-rules countries creative-library-file-types crop-anchor-points
      daily-budget devices device-types isps isp-types languages traffic-types
      timezones marketing-tags-groups marketing-tags minimum-prices
      native-ad-formats network-types offer-algorithms operating-system-types
      operating-systems payment-types pricing-models product-categories proxy-types
      ratescard-formats regions rtb-templates site-types targeting-options
    )

    class << self
      def all
        call(method: :get, path: RESOURCE_PATH)
      end

      COLLECTIONS.each do |collection|
        method_name = collection.gsub('-', '_')

        define_method(method_name) do
          call(method: :get, path: RESOURCE_PATH + '/' + collection)
        end
      end
    end
  end
end
