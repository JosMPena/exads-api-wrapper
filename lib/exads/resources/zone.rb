module Exads
  class Zone < Resource
    RESOURCE_PATH = '/zones'

    class << self
      def all
        call(method: :get, path: RESOURCE_PATH)
      end

      def find(id)
        call(method: :get, path: "#{RESOURCE_PATH}/#{id}")['zone']
      end

      def create(attributes)
        call(method: :post, path: RESOURCE_PATH, params: attributes)
      end

      def snippet(id)
        call(method: :get, path: "#{RESOURCE_PATH}/#{id}/snippet")
      end

      def delete(id)
        call(method: :delete, path: "#{RESOURCE_PATH}/#{id}")
      end
    end
  end
end
