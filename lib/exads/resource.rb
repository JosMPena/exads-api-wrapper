module Exads
  class Resource
    class << self
      private

      def call(*args)
        response = Exads.api_call(*args)

        return response unless response.include? 'result'

        result = response['result']

        result.define_singleton_method(:metadata) do
          response['request_metadata']
        end

        result
      end
    end
  end
end
