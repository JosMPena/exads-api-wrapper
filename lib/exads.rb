require "rest-client"
require 'json'

require "exads/version"
require "exads/errors"
require "exads/configuration"
require "exads/token"
require "exads/resource"

require "exads/resources/collection"
require "exads/resources/zone"

module Exads
  BASE_URL = 'https://api.playfulnetwork.com/v2'

  class << self
    attr_reader :limits

    def configuration
      @configuration ||= Configuration.new
    end

    def configure
      yield(configuration)
    end

    def token
      @token ||= Token.new
    end

    def api_call(method:, path:, params: {}, use_token: true)
      url = Exads::BASE_URL + path

      headers = { 'Content-Type' => 'application/json' }
      headers['Authorization'] = "Bearer #{token.token}" if use_token

      request_params = {
        method: method,
        url: url,
        headers: headers,
        payload: JSON.dump(params)
      }

      begin
        response = RestClient::Request.execute(request_params)
        set_limits(response.headers)
      rescue => e
        e = HttpError.new(
          code: e.http_code,
          headers: e.http_headers,
          response: e.http_body
        ) if e.class.name.start_with?('RestClient::')

        raise(e)
      end

      JSON.parse(response)
    end

    private

    def set_limits(headers)
      @limits = {
        limit: headers[:x_rate_limit_limit],
        remaining: headers[:x_rate_limit_remaining],
        reset: headers[:x_rate_limit_reset],
      }
    end
  end
end
