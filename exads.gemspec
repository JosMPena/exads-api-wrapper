lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "exads/version"

Gem::Specification.new do |spec|
  spec.name          = "exads-api-wrapper"
  spec.version       = Exads::VERSION
  spec.authors       = ["Maksim Novicenko"]
  spec.email         = ["maksim@playfulbet.com"]

  spec.summary       = %q{EXADS API wrapper}
  spec.description   = %q{EXADS API wrapper}
  spec.homepage      = "https://playfulbet.com"
  spec.license       = "MIT"

  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "rest-client", "~> 2.0.2"

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
