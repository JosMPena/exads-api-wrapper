# Exads
EXADS API wrapper

## Configuration
```ruby
Exads.configure do |config|
  config.username = 'username'
  config.password = 'password'
end
```

## Usage example
```ruby
Exads::Zone.all.each do |zone|
  Exads::Zone.snippet(zone['id'])
end
```

## Implemented resources

**Zones**
- all
- find(id)
- create(attributes)
- snippet(id)
- delete(id)

**Collections**
- all
- ad_exchange_partners
- ad_placement_types
- advertiser_ad_types
- publisher_ad_types
- admin_panel_column_settings
- balance_alert
- bidder_rule_settings
- browsers
- campaign_types
- carriers
- categories
- compliance_rules
- countries
- creative_library_file_types
- crop_anchor_points
- daily_budget
- devices
- device_types
- isps
- isp_types
- languages
- traffic_types
- marketing_tags_groups
- marketing_tags
- minimum_prices
- native_ad_formats
- network_types
- offer_algorithms
- operating_system_types
- operating_systems
- payment_types
- pricing_models
- product_categories
- proxy_types
- ratescard_formats
- regions
- rtb_templates
- site_types
- targeting_options
- timezones
